#include <functional>
#include <iostream>
#include "ActionLayer.h"
#include "Perception.h"
#include "Controller.h"

std::function<void (Controller*)> ActionLayer::getAction(Perception& pc)
{
    if (stateOk(pc)) {
        return ([this] (Controller* c) { action(c); });
    }

    if (nextLayer == nullptr) {
        throw NoActionException();
    }

    return nextLayer->getAction(pc);
}

bool ActionLayer::stateOk(Perception& pc)
{
    throw UnspecifiedException();
    return false;
}

void ActionLayer::action(Controller* controller)
{
    throw UnspecifiedException();
}
