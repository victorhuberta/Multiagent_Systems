#include <iostream>
#include "FollowSignal.h"
#include "Perception.h"
#include "Controller.h"

bool FollowSignal::stateOk(Perception& pc)
{
    if (! pc.mothershipCall) {
        return false;
    }

    for (int dir = 0; dir < N_DIRECTIONS; ++dir) {
        Space nextSpace = pc.surroundings[dir];
        bool isBlocked = nextSpace.smallObstacle || nextSpace.bigObstacle ||
            nextSpace.diggingSpot || nextSpace.robot;

        if (isBlocked || nextSpace.invalid) {
            continue;
        }

        if (nextSpace.signalStrength > pc.currentSpace.signalStrength) {
            moveDirection = (Direction) dir;
            return true;
        }
    }
    
    return false;
}

void FollowSignal::action(Controller* controller)
{
    std::cout << "Following signal..." << std::endl;
    try {
        controller->move(moveDirection);
    } catch (ActionNotAllowed& e) {
        std::cout << e.what() << std::endl;
    }
}
