#include <cstdlib>
#include <iostream>
#include "MoveRandomly.h"
#include "Controller.h"

bool MoveRandomly::stateOk(Perception& pc)
{
    freeDirs.clear();

    for (int i = 0; i < N_DIRECTIONS; ++i) {
        bool isBlocked = pc.surroundings[i].smallObstacle ||
            pc.surroundings[i].bigObstacle ||
            pc.surroundings[i].diggingSpot ||
            pc.surroundings[i].robot ||
            pc.surroundings[i].mothership;

        if (! isBlocked && ! pc.surroundings[i].invalid) {
            freeDirs.push_back((Direction) i);
        }
    }

    return (freeDirs.size() > 0);
}

void MoveRandomly::action(Controller* controller)
{
    Direction moveDirection = freeDirs[rand() % freeDirs.size()];
    try {
        controller->move(moveDirection);
        std::cout << "Moved randomly." << std::endl;
    } catch (ActionNotAllowed& e) {
        std::cout << e.what() << std::endl;
    }
}
