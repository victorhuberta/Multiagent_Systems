#ifndef ACTIONLAYER_H
#define ACTIONLAYER_H

#include <functional>
#include "Perception.h"
#include "Controller.h"

class ActionLayer
{
    public:
        ActionLayer() : nextLayer {nullptr} {}
        ~ActionLayer() { if (nextLayer) delete nextLayer; }

        ActionLayer* next(ActionLayer* l) { nextLayer = l; return l; }
        std::function<void (Controller*)> getAction(Perception& pc);

    protected:
        virtual bool stateOk(Perception& pc);
        virtual void action(Controller* controller);

    private:
        ActionLayer* nextLayer;
};

struct NoActionException : public std::exception
{
    const char* what() const noexcept
    {
        return "No suitable action found";
    }
};

struct UnspecifiedException : public std::exception
{
    const char* what() const noexcept
    {
        return "Action layer's behavior unspecified";
    }
};

#endif
