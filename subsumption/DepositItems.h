#ifndef DEPOSITITEMS_H
#define DEPOSITITEMS_H

#include "ActionLayer.h"
#include "Perception.h"
#include "Environment.h"
#include "Controller.h"

#define INVENTORY_CAPACITY 2

class DepositItems : public ActionLayer
{
    public:
        DepositItems() : moveDirection {NORTH}, reached {false} {}

    protected:
        virtual bool stateOk(Perception& pc);
        virtual void action(Controller* controller);

    private:
        Direction moveDirection;
        bool reached;
};

#endif
