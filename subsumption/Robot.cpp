#include <iostream>
#include <functional>
#include "Robot.h"
#include "Perception.h"

Robot::~Robot()
{
    if (layer != nullptr) {
        delete layer;
    }
}

void Robot::fireAction(Perception& pc)
{
    if (layer == nullptr) {
        throw NoActionException();
    }

    try {
        std::function<void (Controller*)> action = layer->getAction(pc);
        action(controller);
    } catch (NoActionException& e) {
        std::cout << e.what() << std::endl;
    }
}
