#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <string>
#include <vector>

#define EMPTY ' '
#define SPACE_BOUNDARY '|'

#define SMALL_OBSTACLE 'o'
#define SMALL_OBSTACLE_POINTS 1

#define BIG_OBSTACLE 'O'
#define BIG_OBSTACLE_POINTS 3

#define DIGGING_SPOT 'x'
#define DIG_POINTS 5

#define CRUMB '.'
#define MOTHERSHIP 'M'
#define ROBOT 'r'

struct Space
{
    bool smallObstacle;
    bool bigObstacle;
    bool diggingSpot;
    bool crumb;
    bool mothership;
    bool robot;
    int signalStrength;
    int points;

    // A flag for spaces outside the environment
    bool invalid;
};

class Environment
{
    public:
        Environment(std::string fn);

        void putObject(char symbol, int x, int y);
        Space getObject(int x, int y);
        int decreasePoints(int points, int x, int y);
        void setSignalStrength(int signal, int x, int y);
        void print();
        void printSignal();

    private:
        std::vector<std::vector<Space>> grid;

        Space updateSpace(Space& space, char symbol);
};

#endif
