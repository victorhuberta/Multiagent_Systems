#ifndef DESTROY_H
#define DESTROY_H

#include "ActionLayer.h"
#include "Perception.h"
#include "Controller.h"

class Destroy : public ActionLayer
{
    public:
        Destroy() : selfOrientation {NORTH} {}

    protected:
        virtual bool stateOk(Perception& pc);
        virtual void action(Controller* controller);

    private:
        Direction selfOrientation;
};

#endif
