#ifndef ROBOT_H
#define ROBOT_H

#include "ActionLayer.h"
#include "Controller.h"

class Robot
{
    public:
        Robot() : layer {nullptr}, controller {nullptr}, active {false} {}
        ~Robot();

        void fireAction(Perception& pc);
        void setController(Controller* c) { controller = c; }
        void setActionLayer(ActionLayer* l) { layer = l; }
        void activate() { active = true; }
        void deactivate() { active = false; }
        bool isActive() { return active;}

    private:
        ActionLayer* layer;
        Controller* controller;
        bool active;
};

#endif
