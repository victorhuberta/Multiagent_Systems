#include <iostream>
#include "Dig.h"
#include "Perception.h"
#include "Controller.h"

bool Dig::stateOk(Perception& pc)
{
    for (int dir = 0; dir < N_DIRECTIONS; ++dir) {
        if (pc.surroundings[dir].diggingSpot) {
            selfOrientation = (Direction) dir;
            return true;
        }
    }

    selfOrientation = pc.selfOrientation;
    return false;
}

void Dig::action(Controller* controller)
{
    std::cout << "Digging for items..." << std::endl;

    try {
        controller->collect(selfOrientation);
        controller->move(selfOrientation);
    } catch (ActionNotAllowed& e) {
        std::cout << e.what() << std::endl;
    }
}
