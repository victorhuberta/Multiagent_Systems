#ifndef FOLLOWSIGNAL_H
#define FOLLOWSIGNAL_H

#include "ActionLayer.h"
#include "Perception.h"
#include "Environment.h"
#include "Controller.h"

class FollowSignal : public ActionLayer
{
    public:
        FollowSignal() : moveDirection {NORTH} {}

    protected:
        virtual bool stateOk(Perception& pc);
        virtual void action(Controller* controller);

    private:
        Direction moveDirection;
};

#endif
