#ifndef MOTHERSHIP_H
#define MOTHERSHIP_H

#include <vector>
#include "Environment.h"
#include "Robot.h"
#include "Controller.h"

#define SHIP_BODY_PARTS 4
const int shipXMods[SHIP_BODY_PARTS] = {0, 0, -1, 1};
const int shipYMods[SHIP_BODY_PARTS] = {0, -1, 0, 0};

#define MAX_SIGNAL 100
#define SUPER_SIGNAL 2000
#define SIGNAL_DELTA 5
#define SIGNAL_NEGATIVE -1
#define SIGNAL_POSITIVE 1

#define TARGET_INVENTORY 20

struct Position
{
    int x;
    int y;
};

class Mothership
{
    public:
        Mothership(Environment* e, int nRobots);
        void land(int xpos, int ypos);
        void placeRobots();
        void activateRobots();
        void runRobots();
        void signalPosition(int signal = MAX_SIGNAL);
        void callRobots();

    private:
        Environment* env;
        std::vector<Robot*> robots;
        std::vector<Controller*> controllers;
        int x, y;
        int inventory;

        std::vector<Position> findEmptyPositions(int minX, int minY,
            int maxX, int maxY);
        void signalVertically(int x, int y, int mod, int signal);
        void signalHorizontally(int x, int y, int mod, int signal);
};

#endif
