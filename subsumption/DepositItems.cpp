#include <iostream>
#include "DepositItems.h"

bool DepositItems::stateOk(Perception& pc)
{
    if (pc.inventory < INVENTORY_CAPACITY) {
        return false;
    }

    for (int dir = 0; dir < N_DIRECTIONS; ++dir) {
        Space nextSpace = pc.surroundings[dir];
        bool isBlocked = nextSpace.smallObstacle || nextSpace.bigObstacle ||
            nextSpace.diggingSpot || nextSpace.robot;

        if (isBlocked || nextSpace.invalid) {
            continue;
        }

        if (nextSpace.signalStrength > pc.currentSpace.signalStrength) {
            moveDirection = (Direction) dir;
            if (nextSpace.mothership) {
                reached = true;
            }
            return true;
        }
    }
    
    return false;
}

void DepositItems::action(Controller* controller)
{
    std::cout << "Putting crumbs... Gotta deposit items!" << std::endl;
    try {
        controller->putCrumb();
        if (reached) {
            reached = false;
            controller->unload(moveDirection);
        } else {
            controller->move(moveDirection);
        }
    } catch (ActionNotAllowed& e) {
        std::cout << e.what() << std::endl;
    }
}
