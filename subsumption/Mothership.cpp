#include <cstdlib>
#include <chrono>
#include <thread>
#include <iostream>
#include "Mothership.h"
#include "Environment.h"
#include "Robot.h"
#include "Controller.h"
#include "MoveRandomly.h"
#include "FollowCrumbs.h"
#include "Dig.h"
#include "DepositItems.h"
#include "FollowSignal.h"
#include "Destroy.h"

Mothership::Mothership(Environment* e, int nRobots)
{
    env = e;
    for (int i = 0; i < nRobots; ++i) {
        controllers.push_back(new Controller(env));
        robots.push_back(new Robot());

        Destroy* actionLayer = new Destroy();
        actionLayer
            ->next(new FollowSignal())
            ->next(new DepositItems())
            ->next(new Dig())
            ->next(new FollowCrumbs())
            ->next(new MoveRandomly());

        robots[i]->setActionLayer(actionLayer);
        robots[i]->setController(controllers[i]);
    }
    x = -1;
    y = -1;
    inventory = 0;
}

void Mothership::land(int xpos, int ypos)
{
    x = xpos;
    y = ypos;

    for (int part = 0; part < SHIP_BODY_PARTS; ++part) {
        int partX = x + shipXMods[part];
        int partY = y + shipYMods[part];

        env->putObject(MOTHERSHIP, partX, partY);
    }
}

void Mothership::placeRobots()
{
    int minX = x, minY = y;
    int maxX = x, maxY = y;
    for (int part = 0; part < SHIP_BODY_PARTS; ++part) {
        int partX = x + shipXMods[part];
        int partY = y + shipYMods[part];

        if (partX < minX) { minX = partX; }
        if (partY < minY) { minY = partY; }

        if (partX > maxX) { maxX = partX; }
        if (partY > maxY) { maxY = partY; }
    }
    
    minX--; minY--; maxX++; maxY++;

    for (int i = 0; i < controllers.size(); ++i) {
        auto emptyPositions = findEmptyPositions(minX, maxX, minY, maxY);
        auto pos = emptyPositions[rand() % emptyPositions.size()];

        int robotX = pos.x, robotY = pos.y;
        controllers[i]->positionRobot(NORTH, robotX, robotY);
    }
}

std::vector<Position> Mothership::findEmptyPositions(int minX, int maxX,
    int minY, int maxY)
{
    std::vector<Position> emptyPos;

    for (int i = minX; i <= maxX; ++i) {
        for (int j = minY; j <= maxY; ++j) {
            Space sp = env->getObject(i, j);

            bool isBlocked = sp.smallObstacle || sp.bigObstacle ||
                sp.diggingSpot || sp.crumb || sp.mothership ||
                sp.robot || sp.invalid;

            if (! isBlocked) {
                Position pos = {i, j};
                emptyPos.push_back(pos);
            }
        }
    }
    return emptyPos;
}

void Mothership::activateRobots()
{
    for (auto robot : robots) {
        robot->activate();
    }
}

void Mothership::runRobots()
{
    int deactivated = 0;

    while (true) {
        env->print();

        for (int i = 0; i < robots.size(); ++i) {
            if (! robots[i]->isActive()) continue;

            Perception pc = controllers[i]->perceive();
            try {
                robots[i]->fireAction(pc);
            } catch (UnloadToMothership& e) {
                std::cout << e.what() << std::endl;
                inventory += e.getInventory();
                if (inventory >= TARGET_INVENTORY) {
                    std::cout << "MOTHERSHIP: Target inventory reached" << std::endl;
                    std::cout << "MOTHERSHIP: Calling all robots..." << std::endl;
                    callRobots();
                }

            } catch (ReturnToMothership& e) {
                std::cout << e.what() << std::endl;

                std::cout << "MOTHERSHIP: Deactivating robot..." << std::endl;
                robots[i]->deactivate();

                ++deactivated;
                if (deactivated >= robots.size()) {
                    return;
                }
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
    }
}

void Mothership::signalPosition(int signal)
{
    Space sp = env->getObject(x, y);
    if (sp.invalid) return;
    env->setSignalStrength(signal, x, y);

    signalHorizontally(x, y, SIGNAL_NEGATIVE, signal);
    signalHorizontally(x, y, SIGNAL_POSITIVE, signal);
    signalVertically(x, y, SIGNAL_NEGATIVE, signal);
    signalVertically(x, y, SIGNAL_POSITIVE, signal);
}

void Mothership::signalVertically(int x, int y, int mod, int signal)
{
    int curY = y;
    int curSignal = signal;

    while (true) {
        curY += mod;
        curSignal -= SIGNAL_DELTA;
        Space sp = env->getObject(x, curY);
        if (sp.invalid) break;

        env->setSignalStrength(curSignal, x, curY);
        signalHorizontally(x, curY, SIGNAL_NEGATIVE, curSignal);
        signalHorizontally(x, curY, SIGNAL_POSITIVE, curSignal);
    }
}

void Mothership::signalHorizontally(int x, int y, int mod, int signal)
{
    int curX = x;
    int curSignal = signal;

    while (true) {
        curX += mod;
        curSignal -= SIGNAL_DELTA;
        Space sp = env->getObject(curX, y);
        if (sp.invalid) break;

        env->setSignalStrength(curSignal, curX, y);
    }
}

void Mothership::callRobots()
{
    signalPosition(SUPER_SIGNAL);
}
