#include <iostream>
#include "Controller.h"
#include "Environment.h"

Controller::Controller(Environment* e)
{
    pc.mothershipCall = false;
    pc.inventory = 0;
    env = e;
    dir = NORTH;
    x = 0;
    y = 0;
}

void Controller::positionRobot(Direction d, int xpos, int ypos)
{
    Space sp = env->getObject(xpos, ypos);
    if (sp.smallObstacle || sp.bigObstacle || sp.diggingSpot ||
        sp.crumb || sp.mothership || sp.robot || sp.invalid) {
        throw InvalidRobotPosition(xpos, ypos);
    }

    env->putObject(ROBOT, xpos, ypos);
    dir = d;
    x = xpos;
    y = ypos;
}

Perception Controller::perceive()
{
    pc.selfOrientation = dir;
    pc.currentSpace = env->getObject(x, y);

    for (int dir = 0; dir < N_DIRECTIONS; ++dir) {
        int sx = x + xmods[dir];
        int sy = y + ymods[dir];
        pc.surroundings[dir] = env->getObject(sx, sy);

        if (pc.surroundings[dir].signalStrength > 999) {
            pc.mothershipCall = true;
        }
    }

    return pc;
}

void Controller::move(Direction dir)
{
    this->dir = dir;
    int nx = x + xmods[dir];
    int ny = y + ymods[dir];

    std::cout << "moving to " << nx << "," << ny << std::endl;

    Space sp = env->getObject(nx, ny);
    if (sp.smallObstacle || sp.bigObstacle || sp.diggingSpot ||
        sp.robot || sp.invalid) {
        throw ActionNotAllowed("There's something blocking its path");
    }

    if (! pc.currentSpace.crumb) {
        env->putObject(EMPTY, x, y);
    }

    if (sp.mothership) {
        throw ReturnToMothership(nx, ny);
    }

    env->putObject(ROBOT, nx, ny);
    x = nx;
    y = ny;
    perceive();
}

void Controller::hit(Direction dir)
{
    this->dir = dir;
    int nx = x + xmods[dir];
    int ny = y + ymods[dir];

    std::cout << "hitting " << nx << "," << ny << std::endl;

    Space sp = env->getObject(nx, ny);
    if (sp.mothership || sp.robot || sp.invalid) {
        throw ActionNotAllowed("Can't hit Mother, fellow robots, "
            "or nothingness");
    }

    if (env->decreasePoints(HIT_DAMAGE, nx, ny) <= 0) {
        env->putObject(EMPTY, nx, ny);
    }
    perceive();
}

void Controller::collect(Direction dir)
{
    this->dir = dir;
    int nx = x + xmods[dir];
    int ny = y + ymods[dir];

    std::cout << "collecting " << nx << "," << ny << std::endl;

    Space sp = env->getObject(nx, ny);
    if (! sp.diggingSpot) {
        throw ActionNotAllowed("Not a digging spot!");
    }

    if (env->decreasePoints(COLLECT_CAPACITY, nx, ny) <= 0) {
        env->putObject(EMPTY, nx, ny);
    }
    pc.inventory += COLLECT_CAPACITY;
    perceive();
}

void Controller::putCrumb()
{
    std::cout << "putting crumb at " << x << "," << y << std::endl;
    env->putObject(CRUMB, x, y);
    perceive();
}

void Controller::unload(Direction dir)
{
    this->dir = dir;
    int nx = x + xmods[dir];
    int ny = y + ymods[dir];

    std::cout << "unloading to " << nx << "," << ny << std::endl;

    Space sp = env->getObject(nx, ny);
    if (! sp.mothership) {
        throw ActionNotAllowed("Can only unload to the mothership");
    }

    int inventory = pc.inventory;
    pc.inventory = 0;
    perceive();
    throw UnloadToMothership(inventory);
}
