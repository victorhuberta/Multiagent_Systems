#ifndef MOVERANDOMLY_H
#define MOVERANDOMLY_H

#include <vector>
#include "ActionLayer.h"
#include "Perception.h"
#include "Environment.h"
#include "Controller.h"

class MoveRandomly : public ActionLayer
{
    protected:
        virtual bool stateOk(Perception& pc);
        virtual void action(Controller* controller);

    private:
        std::vector<Direction> freeDirs;
};

#endif
