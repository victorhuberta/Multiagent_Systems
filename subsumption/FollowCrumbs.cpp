#include <iostream>
#include "FollowCrumbs.h"
#include "Perception.h"
#include "Controller.h"

bool FollowCrumbs::stateOk(Perception& pc)
{
    bool found = false;
    for (int dir = 0; dir < N_DIRECTIONS; ++dir) {
        Space nextSpace = pc.surroundings[dir];
        bool isBlocked = nextSpace.smallObstacle || nextSpace.bigObstacle ||
            nextSpace.diggingSpot || nextSpace.robot ||
            nextSpace.mothership;

        if (isBlocked || nextSpace.invalid) {
            continue;
        }

        if (nextSpace.crumb) {
            if (! found) {
                moveDirection = (Direction) dir;
                found = true;
            } else if (nextSpace.signalStrength < pc.currentSpace.signalStrength) {
                moveDirection = (Direction) dir;
            }
        }
    }
    
    return found;
}

void FollowCrumbs::action(Controller* controller)
{
    std::cout << "Following crumbs..." << std::endl;
    try {
        controller->move(moveDirection);
    } catch (ActionNotAllowed& e) {
        std::cout << e.what() << std::endl;
    }
}
