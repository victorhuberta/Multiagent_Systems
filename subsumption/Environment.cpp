#include <fstream>
#include <sstream>
#include <vector>
#include <iterator>
#include <string>
#include <iostream>
#include "Environment.h"

template<typename Out>
void split(const std::string &s, char delim, Out result) {
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}

Environment::Environment(std::string fn)
{
    std::ifstream envFile(fn);
    std::string line;

    while (std::getline(envFile, line)) {
        auto tokens = split(line, SPACE_BOUNDARY);
        std::vector<Space> row;

        for (auto token : tokens) {
            Space space = {0};
            space = updateSpace(space, token[0]);
            row.push_back(space);
        }
        grid.push_back(row);
    }
}

Space Environment::updateSpace(Space& oldSpace, char symbol)
{
    Space space = {0};
    space.signalStrength = oldSpace.signalStrength;

    switch (symbol) {
        case SMALL_OBSTACLE:
            space.smallObstacle = true;
            space.points = SMALL_OBSTACLE_POINTS;
            break;
        case BIG_OBSTACLE:
            space.bigObstacle = true;
            space.points = BIG_OBSTACLE_POINTS;
            break;
        case DIGGING_SPOT:
            space.diggingSpot = true;
            space.points = DIG_POINTS;
            break;
        case CRUMB:
            space.crumb = true;
            break;
        case MOTHERSHIP:
            space.mothership = true;
            break;
        case ROBOT:
            space.robot = true;
            break;
        default:
            break;
    }

    return space;
}

void Environment::putObject(char symbol, int x, int y)
{
    grid.at(y).at(x) = updateSpace(grid.at(y).at(x), symbol);
}

Space Environment::getObject(int x, int y)
{
    Space sp = {0};
    try {
        sp = grid.at(y).at(x);
    } catch (std::out_of_range& e) {
        sp.invalid = true;
    }
    return sp;
}

int Environment::decreasePoints(int points, int x, int y)
{
    Space& sp = grid.at(y).at(x);
    sp.points -= points;
    return sp.points;
}

void Environment::setSignalStrength(int signal, int x, int y)
{
    Space& sp = grid.at(y).at(x);
    sp.signalStrength = signal;
}

void Environment::print()
{
    for (auto row : grid) {
        for (int i = 0; i < row.size(); ++i) {
            Space& space = row[i];
            char symbol = EMPTY;
            
            if (space.smallObstacle) {
                symbol = SMALL_OBSTACLE;
            } else if (space.bigObstacle) {
                symbol = BIG_OBSTACLE;
            } else if (space.diggingSpot) {
                symbol = DIGGING_SPOT;
            } else if (space.crumb) {
                symbol = CRUMB;
            } else if (space.mothership) {
                symbol = MOTHERSHIP;
            } else if (space.robot) {
                symbol = ROBOT;
            }

            std::cout << symbol;
            if (i != row.size()-1) {
                std::cout << SPACE_BOUNDARY;
            }
        }
        std::cout << std::endl;
    }
}

void Environment::printSignal()
{
    for (auto row : grid) {
        for (int i = 0; i < row.size(); ++i) {
            std::cout << row[i].signalStrength;
            if (i != row.size()-1) {
                std::cout << SPACE_BOUNDARY;
            }
        }
        std::cout << std::endl;
    }
}
