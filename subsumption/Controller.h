#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <string>
#include <sstream>
#include "Environment.h"
#include "Perception.h"

#define HIT_DAMAGE 1
#define COLLECT_CAPACITY 1

const int xmods[N_DIRECTIONS] = {0, 1, 1, 1, 0, -1, -1, -1};
const int ymods[N_DIRECTIONS] = {-1, -1, 0, 1, 1, 1, 0, -1};

struct ActionNotAllowed : public std::exception
{
    public:
        ActionNotAllowed(std::string m) { msg = m; }

        virtual const char* what() const noexcept
        {
            return msg.c_str();
        }

    private:
        std::string msg;
};

struct InvalidRobotPosition : public std::exception
{
    public:
        InvalidRobotPosition(int xpos, int ypos) { x = xpos; y = ypos; }

        virtual const char* what() noexcept
        {
            std::ostringstream oss;
            oss << "Robot can't be at (" << x << ", " << y << ")";
            msg = oss.str();
            return msg.c_str();
        }

    private:
        std::string msg;
        int x, y;
};

struct ReturnToMothership : public std::exception
{
    public:
        ReturnToMothership(int xpos, int ypos) { x = xpos; y = ypos; }

        virtual const char* what() noexcept
        {
            std::ostringstream oss;
            oss << "Robot returned to mothership at (" << x << ", " << y << ")";
            msg = oss.str();
            return msg.c_str();
        }

    private:
        std::string msg;
        int x, y;
};

struct UnloadToMothership : public std::exception
{
    public:
        UnloadToMothership(int inv) { inventory = inv; }

        virtual const char* what() noexcept
        {
            std::ostringstream oss;
            oss << "Robot unloaded inventory of " << inventory;
            msg = oss.str();
            return msg.c_str();
        }

        int getInventory() { return inventory; }
    
    private:
        std::string msg;
        int inventory;
};

class Controller
{
    public:
        Controller(Environment* e);

        void positionRobot(Direction d, int xpos, int ypos);
        Perception perceive();
        void move(Direction dir);
        void hit(Direction dir);
        void collect(Direction dir);
        void putCrumb();
        void unload(Direction dir);

        int x, y;

    private:
        Environment* env;
        Perception pc;
        Direction dir;
};

#endif
