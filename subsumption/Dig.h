#ifndef DIG_H
#define DIG_H

#include "ActionLayer.h"
#include "Perception.h"
#include "Controller.h"

class Dig : public ActionLayer
{
    public:
        Dig() : selfOrientation {NORTH} {}

    protected:
        virtual bool stateOk(Perception& pc);
        virtual void action(Controller* controller);

    private:
        Direction selfOrientation;
};

#endif
