#include <cstdlib>
#include <ctime>
#include "Mothership.h"
#include "Environment.h"

int main(int argc, char** argv)
{
    srand(time(NULL));
    Environment env("environment.txt");
    Mothership ship(&env, 3);
    ship.land(4, 8);
    ship.signalPosition();
    ship.placeRobots();
    ship.activateRobots();
    ship.runRobots();

    return 0;
}
