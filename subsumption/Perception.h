#ifndef PERCEPTION_H
#define PERCEPTION_H

#include "Environment.h"

enum Direction
{
    NORTH = 0,
    NORTH_EAST = 1,
    EAST = 2,
    SOUTH_EAST = 3,
    SOUTH = 4,
    SOUTH_WEST = 5,
    WEST = 6,
    NORTH_WEST = 7,
    N_DIRECTIONS = 8
};

struct Perception
{
    Direction selfOrientation;
    Space currentSpace;
    Space surroundings[N_DIRECTIONS];
    bool mothershipCall;
    int inventory;
};

#endif
