#include <iostream>
#include "Destroy.h"
#include "Perception.h"
#include "Controller.h"

bool Destroy::stateOk(Perception& pc)
{
    selfOrientation = pc.selfOrientation;
    return pc.surroundings[pc.selfOrientation].smallObstacle;
}

void Destroy::action(Controller* controller)
{
    std::cout << "Bashing small rocks..." << std::endl;

    try {
        controller->hit(selfOrientation);
        controller->move(selfOrientation);
    } catch (ActionNotAllowed& e) {
        std::cout << e.what() << std::endl;
    }
}
