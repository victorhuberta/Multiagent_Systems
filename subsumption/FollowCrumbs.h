#ifndef FOLLOWCRUMBS_H
#define FOLLOWCRUMBS_H

#include "ActionLayer.h"
#include "Perception.h"
#include "Environment.h"
#include "Controller.h"

class FollowCrumbs : public ActionLayer
{
    public:
        FollowCrumbs() : moveDirection {NORTH} {}

    protected:
        virtual bool stateOk(Perception& pc);
        virtual void action(Controller* controller);

    private:
        Direction moveDirection;
};

#endif
