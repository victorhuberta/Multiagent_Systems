#include <iostream>
#include "Environment.h"

Environment::Environment(int w, int h)
{
    for (int i = 0; i < h; ++i) {
        std::vector<Square> row;
        for (int j = 0; j < w; ++j) {
            Square sq {0, NORTH, -1};
            row.push_back(sq);
        }
        grid.push_back(row);
    }
}

bool Environment::agent_exists(int agent_id)
{
    for (auto& id : agent_ids) {
        if (agent_id == id) {
            return true;
        }
    }

    return false;
}

void Environment::put_agent(int agent_id, int x, int y, Direction dir)
{
    agent_ids.push_back(agent_id);
    Square& sq = square(x, y);
    sq.agent_id = agent_id;
    sq.agent_dir = dir;
}

void Environment::insert_gold(int gold, int x, int y)
{
    Square& sq = square(x, y);
    sq.gold = gold;
}

void Environment::print_state()
{
    std::cout << std::endl;
    std::cout << "ENVIRONMENT'S STATE" << std::endl;
    std::cout << "agent IDs: " << std::endl;
    for (auto& agent_id : agent_ids) {
        std::cout << agent_id << " ; " << std::endl;
    }
    std::cout << "grid: " << std::endl;
    for (auto& row : grid) {
        for (auto& sq : row) {
            std::cout << "[" << sq.agent_id << "/" << sq.agent_dir << "/" << sq.gold << "] ";
        }
        std::cout << std::endl;
    }
}

Square& Environment::square(int x, int y)
{
    if ((x < 0) || (x >= grid[0].size()) ||
        (y < 0) || (y >= grid.size())) {
        throw NoSuchSquareException();
    }

    return grid[grid.size()-1 - y][x];
}

int Environment::x_pos(int agent_id)
{
    for (auto& row : grid) {
        int x = 0;
        for (auto& square : row) {
            if (square.agent_id == agent_id) {
                return x;
            }
            ++x;
        }
    }
}

int Environment::y_pos(int agent_id)
{
    int y = grid.size()-1;
    for (auto& row : grid) {
        for (auto& square : row) {
            if (square.agent_id == agent_id) {
                return y;
            }
        }
        --y;
    }
}

void Environment::set_pos(int agent_id, int nx, int ny)
{
    int y = grid.size()-1;
    for (auto& row : grid) {
        int x = 0;
        for (auto& square : row) {
            if (square.agent_id == agent_id) {
                square.agent_id = -1;
                square.agent_dir = NORTH;
            }
            if ((x == nx) && (y == ny)) {
                square.agent_id = agent_id;
            }
            ++x;
        }
        --y;
    }
}

Direction Environment::dir(int agent_id)
{
    for (auto& row : grid) {
        for (auto& square : row) {
            if (square.agent_id == agent_id) {
                return square.agent_dir;
            }
        }
    }
}

void Environment::set_dir(int agent_id, Direction new_dir)
{
    for (auto& row : grid) {
        for (auto& square : row) {
            if (square.agent_id == agent_id) {
                square.agent_dir = new_dir;
            }
        }
    }
}

int Environment::get_gold(int agent_id)
{
    for (auto& row : grid) {
        for (auto& square : row) {
            if (square.agent_id == agent_id) {
                return square.gold;
            }
        }
    }
}

void Environment::set_gold(int agent_id, int g)
{
    for (auto& row : grid) {
        for (auto& square : row) {
            if (square.agent_id == agent_id) {
                square.gold = g;
            }
        }
    }
}
