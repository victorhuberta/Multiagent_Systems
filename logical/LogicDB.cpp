#include "LogicDB.h"
#include "Environment.h"

bool LogicDB::at(int x, int y)
{
    return ((x == x_pos) && (y == y_pos));
}

bool LogicDB::facing(Direction d)
{
    return (d == dir);
}

bool LogicDB::gold(int x, int y)
{
    return (((x == x_pos) && (y == y_pos)) && (gold_val > 0));
}
