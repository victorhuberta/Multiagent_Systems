#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <vector>

enum Direction
{
    NORTH,
    EAST,
    SOUTH,
    WEST
};

struct Square
{
    int gold;
    Direction agent_dir;
    int agent_id;
};

struct NoSuchSquareException : public std::exception
{
    const char* what() const noexcept
    {
        return "Target square is NOT in the environment";
    }
};

class Environment
{
    public:
        Environment(int w, int h);
        bool agent_exists(int agent_id);
        void put_agent(int agent_id, int x = 0, int y = 0,
            Direction dir = NORTH);
        void insert_gold(int gold, int x, int y);
        void print_state();

        Square& square(int x, int y);
        int x_pos(int agent_id);
        int y_pos(int agent_id);
        void set_pos(int agent_id, int nx, int ny);

        Direction dir(int agent_id);
        void set_dir(int agent_id, Direction new_dir);

        int get_gold(int agent_id);
        void set_gold(int agent_id, int g);

    private:
        std::vector<std::vector<Square>> grid;
        std::vector<int> agent_ids;
};

#endif
