#include <iostream>
#include "Environment.h"
#include "LogicAgent.h"

int main(int argc, char* argv[])
{
    Environment e {3, 3};
    e.insert_gold(10, 0, 2);
    e.insert_gold(15, 1, 2);

    LogicAgent a {7};
    e.put_agent(a.get_id());

    try {
        a.start(e);
    } catch (NothingToDoException& e) {
        std::cout << e.what() << std::endl;
    }

    e.print_state();
    a.print_db();
    return 0;
}
