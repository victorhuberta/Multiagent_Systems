#ifndef LOGICDB_H
#define LOGICDB_H

#include "Environment.h"

class LogicDB
{
    public:
        LogicDB() : x_pos {-1}, y_pos {-1}, dir {NORTH}, gold_val {0} {}
        bool at(int x, int y);
        bool facing(Direction d);
        bool gold(int x, int y);

        int x() { return x_pos; }
        void set_x(int x) { x_pos = x; }

        int y() { return y_pos; }
        void set_y(int y) { y_pos = y; }

        Direction get_dir() { return dir; }
        void set_dir(Direction d) { dir = d; }

        int get_gold() { return gold_val; }
        void set_gold(int g) { gold_val = g; }

    private:
        int x_pos, y_pos;
        Direction dir;
        int gold_val;
};

#endif
