#ifndef LOGICAGENT_H
#define LOGICAGENT_H

#include "Environment.h"
#include "LogicDB.h"

struct NothingToDoException : public std::exception
{
    const char* what() const noexcept
    {
        return "Agent has nothing else to do";
    }
};

class LogicAgent
{
    public:
        LogicAgent(int _id) { id = _id; carried_gold = 0; }
        int get_id() { return id; }
        int get_carried_gold() { return carried_gold; }

        void start(Environment& e);
        void read(Environment& e);
        void act(Environment& e);
        void pick_up(Environment& e);
        void forward(Environment& e);
        void turn_right(Environment& e);
        void turn_left(Environment& e);
        void print_db();
    
    private:
        int id, carried_gold;
        LogicDB database;
};

#endif
