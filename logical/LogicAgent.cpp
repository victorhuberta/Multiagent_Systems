#include <iostream>
#include "LogicAgent.h"
#include "LogicDB.h"
#include "Environment.h"

void LogicAgent::start(Environment& e)
{
    while (true) {
        read(e);
        act(e);
    }
}

void LogicAgent::read(Environment& e)
{
    if (! e.agent_exists(id))
        return;

    database.set_x(e.x_pos(id));
    database.set_y(e.y_pos(id));
    database.set_dir(e.dir(id));
    database.set_gold(e.get_gold(id));
}

void LogicAgent::act(Environment& e)
{
    LogicDB db = database;
    int x = db.x();
    int y = db.y();

    std::cout << "Agent's at (" << x << ", " << y << ")" << std::endl;

    if (db.at(x, y) && db.gold(x, y)) {
        pick_up(e);
        return;
    }

    if (db.at(0, 2) && db.facing(NORTH)) {
        turn_right(e);
        return;
    }

    if (db.at(1, 2) && db.facing(EAST)) {
        turn_right(e);
        return;
    }

    if (db.at(1, 0) && db.facing(SOUTH)) {
        turn_left(e);
        return;
    }

    if (db.at(2, 0) && db.facing(EAST)) {
        turn_left(e);
        return;
    }

    if (db.at(2, 2) && db.facing(NORTH)) {
        throw NothingToDoException();
    }

    forward(e);
}

void LogicAgent::pick_up(Environment& e)
{
    int found_gold = e.get_gold(id);
    std::cout << "Agent picked up " << found_gold << " Gold" << std::endl;
    carried_gold += found_gold;
    std::cout << "Agent now carries " << carried_gold << " Gold" << std::endl;
    e.set_gold(id, 0);
}

void LogicAgent::forward(Environment& e)
{
    LogicDB db = database;
    int x = db.x();
    int y = db.y();

    if (db.facing(NORTH)) {
        y += 1;
    } else if (db.facing(EAST)) {
        x += 1;
    } else if (db.facing(SOUTH)) {
        y -= 1;
    } else if (db.facing(WEST)) {
        x -= 1;
    }

    try {
        e.square(x, y);
        e.set_pos(id, x, y);
        e.set_dir(id, db.get_dir());
    } catch (NoSuchSquareException& e) {
        std::cout << e.what() << std::endl;
        return;
    }

    std::cout << "Agent moved forward" << std::endl;
}

void LogicAgent::turn_right(Environment& e)
{
    LogicDB db = database;
    if (db.facing(NORTH)) {
        e.set_dir(id, EAST);
    } else if (db.facing(EAST)) {
        e.set_dir(id, SOUTH);
    } else if (db.facing(SOUTH)) {
        e.set_dir(id, WEST);
    } else if (db.facing(WEST)) {
        e.set_dir(id, NORTH);
    }

    std::cout << "Agent turned right" << std::endl;
}

void LogicAgent::turn_left(Environment& e)
{
    LogicDB db = database;
    if (db.facing(NORTH)) {
        e.set_dir(id, WEST);
    } else if (db.facing(EAST)) {
        e.set_dir(id, NORTH);
    } else if (db.facing(SOUTH)) {
        e.set_dir(id, EAST);
    } else if (db.facing(WEST)) {
        e.set_dir(id, SOUTH);
    }

    std::cout << "Agent turned left" << std::endl;
}

void LogicAgent::print_db()
{
    LogicDB db = database;
    std::cout << std::endl;
    std::cout << "AGENT " << id << "'S PERCEPTION" << std::endl;
    std::cout << "x: " << db.x() << std::endl;
    std::cout << "y: " << db.y() << std::endl;
    std::cout << "direction: " << db.get_dir() << std::endl;
    std::cout << "gold: " << db.get_gold() << std::endl;
}
